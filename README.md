# Deltares RTC-Tools

The rtc-tools repository has been moved to github: https://github.com/Deltares/rtc-tools.

To change your git remote url, run

`git remote set-url origin https://github.com/Deltares/rtc-tools.git`

and

`git remote set-url --push origin https://github.com/Deltares/rtc-tools.git`.
